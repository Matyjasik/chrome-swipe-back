console.log("Plugin initialized");

var blockSwipeTemporarily = false;

document.addEventListener("mousewheel", function(e){
    if (!blockSwipeTemporarily && isHorizontalSwipe()){
        if (isForward()) {
            console.log("Navigating forward");
            history.forward();
        } else { 
            console.log("Navigating back");
            history.back();
        }

        blockSwipeTemporarily = true;
        setTimeout(function() {
            blockSwipeTemporarily = false; 
        }, 1000);
    }

    function isHorizontalSwipe(){
        return Math.abs(e.deltaY) < 0.1 && Math.abs(e.deltaX) > 1;
    }

    function isForward(){
        return e.deltaX > 0;
    }
});